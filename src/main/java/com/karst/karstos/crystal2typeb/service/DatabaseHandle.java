package com.karst.karstos.crystal2typeb.service;

import com.karst.karstos.crystal2typeb.entities.api.ApiContext;
import com.karst.karstos.crystal2typeb.entities.api.ApiExtensionData;
import com.karst.karstos.crystal2typeb.entities.databese.DataSource;
import com.karst.karstos.crystal2typeb.entities.response.DbResponseResult;
import com.karst.karstos.crystal2typeb.entities.sql.SqlInfo;
import com.karst.karstos.crystal2typeb.service.impl.*;
import org.springframework.stereotype.Service;

/**
 * Created by TianLedao on 2018/4/25.
 */
@Service
public class DatabaseHandle {

    public DataBase createDataBase(String dbType){
        DataBase dataBase = null;
        switch(dbType){
            case "mysql":
                dataBase = new Mysql();
                break;
            case "oracle":
                dataBase = new Oracle();
                break;
            case "mssql":
                dataBase = new SqlServer();
                break;
            case "db2":
                dataBase = new IBMDB2();
                break;
            case "pgsql":
                dataBase = new PostgreSQL();
                break;
        }
        return dataBase;
    }

    public DbResponseResult dbHandle(ApiContext apiContext){
        ApiExtensionData apiExtensionData = apiContext.getApiExtensionData();
        DataSource dataSource = apiExtensionData.getDataSource();
        SqlInfo sqlInfo = apiExtensionData.getSqlInfo();
        //根据数据库类型判断该实例化哪种数据库操作对象
        String dbType = dataSource.getDbType();
        DataBase dataBase = createDataBase(dbType);

        DbResponseResult dbResponseResult = dataBase.connectDB(apiContext.getRequestPath(),dataSource);

        if (dbResponseResult != null){
            return dbResponseResult;
        }

        String sqltype = sqlInfo.getSqltype();
        switch(sqltype){
            case "get":
                dbResponseResult = dataBase.readRow(sqlInfo);
                break;
            case "list":
                dbResponseResult = dataBase.readList(sqlInfo);
                break;
            case "create":
                dbResponseResult = dataBase.insert(sqlInfo);
                break;
            case "update":
                dbResponseResult = dataBase.update(sqlInfo);
                break;
            case "delete":
                dbResponseResult = dataBase.delete(sqlInfo);
                break;
        }

        return dbResponseResult;
    }

}
