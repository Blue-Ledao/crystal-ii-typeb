package com.karst.karstos.crystal2typeb.entities;

import com.atlassian.oai.validator.SwaggerRequestResponseValidator;


/**
 * Created by TianLedao on 2018/4/25.
 */

public class SwaggerSpecEntity {
    private String swaggerContext;
    private SwaggerRequestResponseValidator validator;
    private Error error;

    public SwaggerSpecEntity() {

    }

    public SwaggerSpecEntity(Error error) {
        this.error = error;
    }

    public SwaggerSpecEntity(String swaggerContext) {
        this.swaggerContext = swaggerContext;
        this.validator = SwaggerRequestResponseValidator.createFor(swaggerContext).build();
    }

    public String getSwaggerContext() {
        return swaggerContext;
    }

    public void setSwaggerContext(String swaggerContext) {
        this.swaggerContext = swaggerContext;
    }

    public SwaggerRequestResponseValidator getValidator() {
        return validator;
    }

    public void setValidator(SwaggerRequestResponseValidator validator) {
        this.validator = validator;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
