package com.karst.karstos.crystal2typeb.entities.constants;

/**
 * Created by TianLedao on 2018/4/25.
 */
public class MessageCode {
    public static final int SUCCESSFULLY_CREATE_DATA_CODE = 200101;
    public static final String SUCCESSFULLY_CREATE_DATA = "成功添加了数据";

    public static final int SUCCESSFULLY_QUERY_DATA_CODE = 200102;
    public static final String SUCCESSFULLY_QUERY_DATA = "查询数据成功";

    public static final int GET_EMPTY_RESULTSETS_CODE = 200108;
    public static final String GET_EMPTY_RESULTSETS = "查询结果为空";

    public static final int SUCCESSFULLY_UPDATE_DATA_CODE = 200103;
    public static final String SUCCESSFULLY_UPDATE_DATA = "成功更新了数据";

    public static final int NO_DATA_WAS_UPDATE_CODE = 200105;
    public static final String NO_DATA_WAS_UPDATE = "没有数据被修改";

    public static final int SUCCESSFULLY_DELETE_DATA_CODE = 200104;
    public static final String SUCCESSFULLY_DELETE_DATA = "成功删除了数据";

    public static final String SWAGGER_ERROR = "swagger error";

    public static final int SWAGGER_VALIDATION_FAILURE_CODE = 403101;
    public static final String SWAGGER_VALIDATION_FAILURE = "swagger validation failure";

    public static final int API_ID_OR_API_VERSION_MISSING_CODE = 403102;
    public static final String API_ID_OR_API_VERSION_MISSING = "%s and %s in header is required";


    public static final String DATABASE_ERROR = "数据库错误";

    public static final int DRIVER_LOADING_ERROR_CODE = 500101;
    public static final String DRIVER_LOADING_ERROR = "数据库驱动载入错误";

    public static final int DATABASE_CONNECT_ERROR_CODE = 500102;
    public static final String DATABASE_CONNECT_ERROR = "数据库连接错误";

    public static final int SQL_STATEMENT_ERROR_CODE = 500103;
    public static final String SQL_STATEMENT_ERROR = "sql语句错误";

    public static final int MISSING_SCHEMA_ERROR_CODE = 500104;
    public static final String MISSING_SCHEMA_ERROR = "缺失schema";

    public static final int COUNT_TOTAL_PAGE_NUMBER_ERROR_CODE = 400101;
    public static final String COUNT_TOTAL_PAGE_NUMBER_ERROR = "计算页数错误";
}
