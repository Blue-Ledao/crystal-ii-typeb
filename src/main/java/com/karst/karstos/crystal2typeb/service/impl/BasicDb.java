package com.karst.karstos.crystal2typeb.service.impl;

import com.karst.karstos.crystal2typeb.entities.api.ApiExtensionData;
import com.karst.karstos.crystal2typeb.entities.constants.MessageCode;
import com.karst.karstos.crystal2typeb.entities.databese.Column;
import com.karst.karstos.crystal2typeb.entities.databese.DataSource;
import com.karst.karstos.crystal2typeb.entities.error.Error;
import com.karst.karstos.crystal2typeb.entities.response.DbResponseResult;
import com.karst.karstos.crystal2typeb.entities.sql.SqlInfo;
import com.karst.karstos.crystal2typeb.service.DataBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Created by TianLedao on 2018/4/28.
 */
public abstract class BasicDb implements DataBase {

    public Logger log = LoggerFactory.getLogger(this.getClass());

    public Connection connection;
    public PreparedStatement preparedStatement;
    public CallableStatement callableStatement;
    public Statement statement;
    public ResultSet resultSet;
    public ResultSetMetaData resultSetMetaData;
    public String jdbcURl;
    public String quotationMark;

    @Override
    public DbResponseResult connectDB(String requestPath, DataSource dbConnectInfo) {
        try {
            this.connection = DriverManager.getConnection(this.jdbcURl, dbConnectInfo.getUsername(), dbConnectInfo.getPassword());
            this.statement = connection.createStatement();
        } catch (SQLException e) {
            Error error = new Error(e.getMessage(), MessageCode.DRIVER_LOADING_ERROR_CODE,
                    MessageCode.DATABASE_ERROR,requestPath,new java.util.Date());
            return new DbResponseResult(error);
        }
        return null;
    }

    @Override
    public DbResponseResult readRow(SqlInfo sqlInfo) {
        return null;
    }

    @Override
    public DbResponseResult readList(SqlInfo sqlInfo) {
        return null;
    }

    @Override
    public DbResponseResult insert(SqlInfo sqlInfo) {
        return null;
    }

    @Override
    public DbResponseResult update(SqlInfo sqlInfo) {
        return null;
    }

    @Override
    public DbResponseResult delete(SqlInfo sqlInfo) {
        return null;
    }

    public <T> T convertedToJdbcDataType(ResultSet resultSet,String columnName,int dataType) throws SQLException{
        Object result = null;
        //���·����Ǹ���jdbc��type�ӿڶ�����������ͽ��еķ���
        switch (dataType){
            //String
            case -16:
            case -15:
            case -9:
            case -8:
            case -1:
            case 0:
            case 1:
            case 12:
            case 91:
            case 92:
            case 93:
            case 1111:
            case 2005:
            case 2009:
            case 2011:
            case 2013:
            case 2014:
                result = resultSet.getString(columnName);
                break;
            //Blob����
            case 2004:
                result = resultSet.getBlob(columnName);
                break;
            //����������byte[]
            case -4:
            case -3:
            case -2:
                byte[] byteArray = resultSet.getBytes(columnName);
                if (byteArray != null){
                    result = new String(byteArray);
                }
                break;
            //boolean
            case -7:
            case 16:
                result = resultSet.getBoolean(columnName);
                break;
            //int
            case -6:
            case 4:
            case 5:
                result = resultSet.getInt(columnName);
                break;
            //long
            case -5:
                result = resultSet.getLong(columnName);
                break;
            //float
            case 7:
                result = resultSet.getFloat(columnName);
                break;
            //double
            case 6:
            case 8:
                result = resultSet.getDouble(columnName);
                break;
            //bigDecimal
            case 2:
            case 3:
                result = resultSet.getBigDecimal(columnName);
                break;
            //ˮ��������֧�ֵ����ݾ�����null��
            default:
                result = "��֧�ֵ���������";
                break;
        }

        return (T)result;
    }

    public String buildBasicQuerySql(SqlInfo sqlInfo){
        List<Column> columnList = sqlInfo.getCommonColumns();
        sqlInfo.getTableType();
        String querySql = "select ";
        String schema = sqlInfo.getSchema();
        String tableName;
        List<String> tableNames = sqlInfo.getTableNames();

        tableName = buildTable(schema,tableNames);

        if (sqlInfo.isEnablePaging()){
            countTotalPageNum(sqlInfo,tableName);
        }
        for(int i = 0;i < columnList.size();i++){
            Column column = columnList.get(i);
            if(i > 0){
                querySql += ",";
            }
            querySql +=  column.getColumnName() + " as " + column.getAlias();
        }
        querySql += " from " + tableName;
        return querySql;
    }

    public abstract <T> T dataTypeConvertsion(ResultSet resultSet, String columnName, String columnTypeName,int dataType) throws SQLException;

    public DbResponseResult executeQuery(String querySql) {
        DbResponseResult dbResponseResult = new DbResponseResult();

        List<HashMap<String,Object>> resultList = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(querySql);
            resultSet = preparedStatement.executeQuery();
            resultSetMetaData = resultSet.getMetaData();
            while(resultSet.next()){
                HashMap<String,Object> columnDataMap = new HashMap();
                for(int i = 1;i <= resultSetMetaData.getColumnCount();i++){
                    int dataType = resultSetMetaData.getColumnType(i);
                    String columnName = resultSetMetaData.getColumnLabel(i);
                    String columnTypeName = resultSetMetaData.getColumnTypeName(i);
                    Object result = dataTypeConvertsion(resultSet,columnName,columnTypeName,dataType);
                    columnDataMap.put(columnName,result);
                }
                resultList.add(columnDataMap);
            }
            preparedStatement.close();
            resultSet.close();
            connection.close();
        } catch (SQLException e) {
            Error error = new Error(e.getMessage(), MessageCode.SQL_STATEMENT_ERROR_CODE,
                    MessageCode.SQL_STATEMENT_ERROR,null,new java.util.Date());
            dbResponseResult.setError(error);
            return dbResponseResult;
        }
        if(resultList.size()==0){
            dbResponseResult.setMessage(MessageCode.GET_EMPTY_RESULTSETS);
            dbResponseResult.setCode(MessageCode.GET_EMPTY_RESULTSETS_CODE);
            return dbResponseResult;
        }
        dbResponseResult.setResultList(resultList);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_QUERY_DATA_CODE);
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_QUERY_DATA);

        return dbResponseResult;
    }

    public void countTotalPageNum(SqlInfo sqlInfo,String tableName) {
        int totalPageNum = 0;
        int rowNum = 0;
        String countSql = "select count(*) from " + tableName + sqlInfo.getWhereClause();
        log.info(countSql);
        try {
            preparedStatement = connection.prepareStatement(countSql);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            rowNum = resultSet.getInt(1);
            totalPageNum = (rowNum + sqlInfo.getEndOfQuery() - 1) / sqlInfo.getPageSize();
        } catch (SQLException e) {
            Error error = new Error(e.getMessage(), MessageCode.COUNT_TOTAL_PAGE_NUMBER_ERROR_CODE,
                    MessageCode.COUNT_TOTAL_PAGE_NUMBER_ERROR, null, new java.util.Date());
            sqlInfo.setBuildingStstus(error);
        }
        int start = (sqlInfo.getCurrentPageNum() - 1) * sqlInfo.getPageSize();
        int end = sqlInfo.getPageSize() * sqlInfo.getCurrentPageNum();
        sqlInfo.setTotalPageNum(totalPageNum);
        sqlInfo.setStartOfQuery(start);
        sqlInfo.setEndOfQuery(end);
    }

    public static String getOperator(String operationType){
        String operator = "";
        switch (operationType) {
            case "gt":
                operator = ">";
                break;
            case "gte":
                operator = ">=";
                break;
            case "lt":
                operator = "<";
                break;
            case "lte":
                operator = "<=";
                break;
            case "eq":
                operator = "=";
                break;
            case "ne":
                operator = "!=";
                break;
            default:
                operator = operationType;
        }
        return operator;
    }

    public void builderSql(SqlInfo sqlInfo){
        String sqlType = sqlInfo.getSqltype();
        String schema = sqlInfo.getSchema();
        List<String> tableNames = sqlInfo.getTableNames();
        List<Column> columnList =  sqlInfo.getCommonColumns();
        String tableName = buildTable(schema,tableNames);
        String sql;
        String whereClause;
        switch(sqlType) {
            case "get":
                sql = buildBasicQuerySql(sqlInfo);
                whereClause = sqlInfo.getWhereClause();
                if(whereClause!=null && !whereClause.equals("")){
                    sql += whereClause;
                }
                sqlInfo.setSql(String.format(sqlInfo.getSql(), sql));
                break;
            case "list":
                sql = buildBasicQuerySql(sqlInfo);
                whereClause = sqlInfo.getWhereClause();
                if(whereClause!=null && !whereClause.equals("")){
                    sql += whereClause;
                }
                if (sqlInfo.isEnablePaging()) {
                    int start = sqlInfo.getStartOfQuery();
                    int end = sqlInfo.getEndOfQuery();
                    if(sqlInfo.getBuildingStstus() != null){
                        break;
                    }
                    if(sqlInfo.getDbType().equals("mysql")){
                        sqlInfo.setSql(String.format(sqlInfo.getSql(), sql,start,end));
                    }else{
                        sqlInfo.setSql(String.format(sqlInfo.getSql(), sql,end,start));
                    }
                } else {
                    sqlInfo.setSql(String.format(sqlInfo.getSql(), sql));
                }
                break;
            case "create":
                String columnNames = "(";
                String values = "(";
                sql = "insert into " + tableName;
                for (int i = 0; i < columnList.size(); i++) {
                    Column column = columnList.get(i);
                    if (i > 0) {
                        columnNames += ",";
                        values += ",";
                    }
                    columnNames += column.getColumnName();

                    if (column.isDefaultColumn()) {
                        values += defaultParameterSetting(column);
                    } else {
                        String value = column.getColumnValue();
                        values += value;
                    }
                }
                sql += columnNames + ")" + "values" + values + ")";
                sqlInfo.setSql(sql);
                break;
            case "update":
                sql = "update " + tableName + " set ";
                for(int i = 0;i < columnList.size();i++){
                    String value = "";
                    Column column = columnList.get(i);
                    if(i > 0){
                        sql += ",";
                    }
                    value = column.getColumnValue();
                    sql += column.getColumnName() + " = " + value;
                }
                whereClause = sqlInfo.getWhereClause();
                if(whereClause!=null && !whereClause.equals("")){
                    sql += whereClause;
                }
                sqlInfo.setSql(sql);
                break;
            case "delete":
                sql = "delete from " + tableName;
                whereClause = sqlInfo.getWhereClause();
                if(whereClause!=null && !whereClause.equals("")){
                    sql += whereClause;
                }
                sqlInfo.setSql(sql);
                break;
        }
    }

    public DbResponseResult executeWriter(String sql){
        DbResponseResult dbResponseResult = new DbResponseResult();

        List<Map<String,Object>> resultList = new ArrayList<>();
        int affectedRowNum;
        try {
            preparedStatement = connection.prepareStatement(sql);
            affectedRowNum = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            Error error = new Error(sql+e.getMessage(), MessageCode.SQL_STATEMENT_ERROR_CODE,
                    MessageCode.SQL_STATEMENT_ERROR,null,new Date());
            dbResponseResult.setError(error);
            return dbResponseResult;
        }
        dbResponseResult.setAffectedRowNum(affectedRowNum);
        return dbResponseResult;
    }

    public abstract  String defaultParameterSetting(Column column);

    public String buildTable(String schema ,List<String> tableNames){
        String tableName = "";
        if (schema != null && !schema.equals("")){
            for(int i = 0; i < tableNames.size();i++){
                if(i>1){
                    tableName += ",";
                }
                tableName += schema + "." + tableNames.get(i);
            }
        }else {
            for(int i = 0; i < tableNames.size();i++){
                if(i>1){
                    tableName += ",";
                }
                tableName += tableNames.get(i);
            }
        }
        return tableName;
    }
}
