package com.karst.karstos.crystal2typeb.entities.api;

import com.karst.karstos.crystal2typeb.entities.databese.DataSource;
import com.karst.karstos.crystal2typeb.entities.sql.SqlInfo;

/**
 * Created by TianLedao on 2018/4/26.
 */
public class ApiExtensionData {
    private String sqlId;
    private String dbSourceId;
    private String responseType;
    private DataSource dataSource;
    private SqlInfo sqlInfo;

    public String getSqlId() {
        return sqlId;
    }

    public void setSqlId(String sqlId) {
        this.sqlId = sqlId;
    }

    public String getDbSourceId() {
        return dbSourceId;
    }

    public void setDbSourceId(String dbSourceId) {
        this.dbSourceId = dbSourceId;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public SqlInfo getSqlInfo() {
        return sqlInfo;
    }

    public void setSqlInfo(SqlInfo sqlInfo) {
        this.sqlInfo = sqlInfo;
    }
}
