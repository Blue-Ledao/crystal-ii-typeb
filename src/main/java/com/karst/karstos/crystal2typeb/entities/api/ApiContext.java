package com.karst.karstos.crystal2typeb.entities.api;

import com.atlassian.oai.validator.model.ApiOperation;
import com.atlassian.oai.validator.model.Request.Method;
import io.swagger.models.parameters.Parameter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by TianLedao on 2018/4/26.
 */

public class ApiContext {
    public static String DATA_SOURCE ="x-dataSource";
    public static String DATA_SOURCE_ID ="x-dataSourceId";
    public static String REQUEST_SQL ="x-requestSql";
    public static String REQUEST_SQL_ID ="x-requestSqlId";
    public static String RESPONSE_TYPE ="x-responseType";

    private String requestPath;
    private Method method;
    private String bodyParam;
    private ApiOperation apiOperation;
    private ApiExtensionData apiExtensionData;
    private Map<String,Object> swaggerVendorExtensions = new HashMap<>();
    private Map<String,String> apiRequestParamMap = new HashMap<>();

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    public ApiOperation getApiOperation() {
        return apiOperation;
    }

    public void setApiOperation(ApiOperation apiOperation) {
        this.apiOperation = apiOperation;
    }

    public ApiExtensionData getApiExtensionData() {
        return apiExtensionData;
    }

    public void setApiExtensionData(ApiExtensionData apiExtensionData) {
        this.apiExtensionData = apiExtensionData;
    }

    public Map<String, Object> getSwaggerVendorExtensions() {
        return swaggerVendorExtensions;
    }

    public void setSwaggerVendorExtensions(Map<String, Object> swaggerVendorExtensions) {
        this.swaggerVendorExtensions = swaggerVendorExtensions;
    }

    public Map<String, String> getApiRequestParamMap() {
        return apiRequestParamMap;
    }

    /*-----------------getter、setter分割线----------------------*/

    public void addApiRequestParam(String paramName, String paramLocation, String paramValue){
        String key = paramName + "&" + paramLocation;
        apiRequestParamMap.put(key,paramValue);
    }

    public String getApiRequestParam(String paramName, String paramLocation){
        String key = paramName + "&" + paramLocation;
        return apiRequestParamMap.get(key);
    }

    public List<Parameter> getParameters(){
        return this.getApiOperation().getOperation().getParameters();
    }

    public Map<String,Object> getApiOperationVendorExtensions(){
        return this.getApiOperation().getOperation().getVendorExtensions();
    }
}
