package com.karst.karstos.crystal2typeb.service;

import com.atlassian.oai.validator.model.ApiOperation;
import com.karst.karstos.crystal2typeb.entities.api.ApiContext;
import com.karst.karstos.crystal2typeb.entities.api.ApiExtensionData;
import com.karst.karstos.crystal2typeb.entities.constants.HttpConstants;
import com.karst.karstos.crystal2typeb.entities.databese.Column;
import com.karst.karstos.crystal2typeb.entities.databese.DataSource;
import com.karst.karstos.crystal2typeb.entities.sql.SqlInfo;
import com.karst.karstos.crystal2typeb.service.columns.BindColumnsData;
import io.swagger.models.parameters.FormParameter;
import io.swagger.models.parameters.HeaderParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.parameters.QueryParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by TianLedao on 2018/4/25.
 */
@Service
public class ApiParamBinder {

    @Autowired
    BindColumnsData bindColumnsData;

    public void parseHttpRequestParam(HttpServletRequest request, ApiContext apiContext){

        String paramValue = null;
        //这个parameters是swagger里面定义的parameter
        List<Parameter> parameters = apiContext.getParameters();
        boolean hasParsePathParam = false;
        for (Parameter parameter: parameters) {
            //apiContext.putLocationMap(parameter.getName(),parameter.getIn());
            switch (parameter.getIn()) {
                case HttpConstants.API_OPERATION_PARAM_FROM_FORMDATA:
                    paramValue = request.getParameter(parameter.getName());
                    //尝试读取swagger参数中的默认值
                    if(paramValue == null && ((FormParameter)parameter).getDefaultValue() != null){
                        paramValue = paramDefaultValueToString(((FormParameter)parameter).getDefaultValue());
                    }
                    if(paramValue != null){
                        apiContext.addApiRequestParam(parameter.getName(), parameter.getIn(),paramValue);
                    }
                    break;
                case HttpConstants.API_OPERATION_PARAM_FROM_QUERY:
                    paramValue = request.getParameter(parameter.getName());
                    if(paramValue == null && ((QueryParameter)parameter).getDefaultValue() != null){
                        paramValue = paramDefaultValueToString(((QueryParameter)parameter).getDefaultValue());
                    }
                    if(paramValue != null){
                        apiContext.addApiRequestParam(parameter.getName(), parameter.getIn(), paramValue);
                    }
                    break;
                case HttpConstants.API_OPERATION_PARAM_FROM_HEADER:
                    paramValue = request.getHeader(parameter.getName());
                    if(paramValue == null && ((HeaderParameter)parameter).getDefaultValue() != null){
                        paramValue = paramDefaultValueToString(((HeaderParameter)parameter).getDefaultValue());
                    }
                    if(paramValue != null){
                        apiContext.addApiRequestParam(parameter.getName(), parameter.getIn(), paramValue);
                    }
                    break;
                case HttpConstants.API_OPERATION_PARAM_FROM_PATH:
                    if (!hasParsePathParam) {
                        parsePathParam(apiContext);
                        hasParsePathParam = true;
                    }
                    break;
            }
        }
    }


    private DataSource bindDataSource(Map<String,Object> dbConnection){
        DataSource dataSource = new DataSource();
        dataSource.setModifyTimestamp((String) dbConnection.get("modifyTimestamp"));
        dataSource.setHost((String) dbConnection.get("host"));
        dataSource.setPort((String) dbConnection.get("port"));
        dataSource.setDbType((String) dbConnection.get("dbType"));
        dataSource.setDbName((String) dbConnection.get("dbName"));
        dataSource.setUsername((String) dbConnection.get("username"));
        dataSource.setPassword((String) dbConnection.get("password"));
        System.out.println("你好" + dbConnection.get("isOpen"));
        dataSource.setOpen(Boolean.parseBoolean("true"));
        return dataSource;
    }

    /*private List<Column> bindColumns(List<Map<String,String>> columns){
        for(Map<String,String> columnMap:columns){
            Column column = new Column();

            //判断该Column是否被设置成了默认Column,
            // 默认column的value和非默认column的处理方式不同
            if(columnMap.containsKey("defaultColumn")){
                column.setDefaultColumn(Boolean.parseBoolean(columnMap.get("defaultColumn")));
                column.setColumnValue(columnMap.get("value"));
                if(columnMap.get("defaultValueType").equals(Column.DYNAMIC_DEFAULT_VALUE)){

                }

            }else{

            }

            column.setBelongToTable(columnMap.get("belongToTable") != null ? columnMap.get("belongToTable") : null);
            column.setColumnName(columnMap.get("columnName") != null ? columnMap.get("columnName") : null);
            column.setColumnType(columnMap.get("columnType") != null ? columnMap.get("columnType") : null);
            column.setAlias(columnMap.get("alias") != null ? columnMap.get("alias") : null);
            column.setOutputFormat(columnMap.get("outputFormat") != null ? columnMap.get("outputFormat") : null);
            column.setDefaultColumn(columnMap.get("defaultColumn") != null ? Boolean.parseBoolean(columnMap.get("defaultColumn")) : null);
            column.setDefaultValueType(columnMap.get("belongToTable") != null ? columnMap.get("belongToTable") : null);


        }


        return null;
    }*/


    private void parsePathParam(ApiContext apiContext) {
        String path;
        ApiOperation apiOperation = apiContext.getApiOperation();
        Set<Map.Entry<String, Optional<String>>> paramValues;
        for (int i = 0; i < apiOperation.getApiPath().numberOfParts(); i++) {
            path = apiOperation.getRequestPath().part(i);
            paramValues = apiOperation.getApiPath().paramValues(i, path).entrySet();
            for (Map.Entry<String, Optional<String>> v : paramValues) {
                if (v.getValue().isPresent()) {
                    apiContext.addApiRequestParam(v.getKey(), HttpConstants.API_OPERATION_PARAM_FROM_PATH, v.getValue().get());
                }
            }
        }
    }

    public void parseVendorExtensions(ApiContext apiContext,HttpServletRequest request){
        ApiExtensionData apiExtensionData = new ApiExtensionData();

        Map<String,Object> swaggerVendorExtensions  = apiContext.getSwaggerVendorExtensions();
        Map<String,Object> apiOperationVendorExtensions = apiContext.getApiOperationVendorExtensions();
        Map<String,Object> dataSourceMap = (Map<String, Object>) swaggerVendorExtensions.get(ApiContext.DATA_SOURCE);
        Map<String,Object> sqlMap = (Map<String, Object>) swaggerVendorExtensions.get(ApiContext.REQUEST_SQL);
        String sqlId = "sql_No." + apiOperationVendorExtensions.get(ApiContext.REQUEST_SQL_ID);
        String dbSourceId = "dataSource_No." + apiOperationVendorExtensions.get(ApiContext.DATA_SOURCE_ID);

        apiExtensionData.setResponseType((String) apiOperationVendorExtensions.get(ApiContext.RESPONSE_TYPE));

        Map<String,Object> dbConnection = (Map<String,Object>) dataSourceMap.get(dbSourceId);
        DataSource dataSource = bindDataSource(dbConnection);
        apiExtensionData.setDataSource(dataSource);

        SqlInfo sqlInfo  = new SqlInfo();
        Map<String,Object> sql = (Map<String,Object>) sqlMap.get(sqlId);
        sqlInfo.setDbType(dataSource.getDbType());
        sqlInfo.setModifyTimestamp((String) sql.get("modifyTimestamp"));
        sqlInfo.setSqltype((String) sql.get("sqlType"));
        sqlInfo.setTableType((String) sql.get("tableType"));
        sqlInfo.setEnablePaging((Boolean) sql.get("enablePaging"));


        Map<String, String> apiRequestParamMap = apiContext.getApiRequestParamMap();
        if(sqlInfo.isEnablePaging()){
            sqlInfo.setPageSize(Integer.parseInt(apiRequestParamMap.get(sql.get("pageSize"))));
            sqlInfo.setCurrentPageNum(Integer.parseInt(apiRequestParamMap.get(sql.get("pageNumber"))));
        }

        List<Map<String,Object>> columnList = (List<Map<String,Object>>)sql.get("columns");
        List<Column> commonColumns = null;
        switch (sqlInfo.getSqltype()){
            case "get":
            case "list":
                commonColumns = bindColumnsData.buildColumnsTypeOfQuery(dataSource.getDbType(),columnList);
                break;
            case "create":
                commonColumns = bindColumnsData.buildColumnsTypeOfCreate(dataSource.getDbType(), columnList, apiContext);
                break;
            case "update":
                commonColumns = bindColumnsData.buildColumnsTypeOfUpdate(dataSource.getDbType(), columnList, apiContext);
                break;
        }
        sqlInfo.setCommonColumns(commonColumns);

        List<String> tableNames = (List<String>) sql.get("tableNames");

        for (int i = 0; i< tableNames.size();i++){
            String tableName = bindColumnsData.meteDataPreprocess(dataSource.getDbType(), tableNames.get(i),null,null);
            tableNames.set(i,tableName);
        }
        sqlInfo.setTableNames(tableNames);
        String schema = bindColumnsData.meteDataPreprocess(dataSource.getDbType(),(String) sql.get("shema"),null,null);
        sqlInfo.setSchema(schema);

        List<Map<String,?>> conditions = (List<Map<String,?>>) sql.get("conditions");



        List<Column> columnsOfWhereClause = new ArrayList<>();
        if(conditions != null){
            String whereClause =bindColumnsData.bindParamForWhereClause(dataSource.getDbType(),conditions, columnsOfWhereClause, apiRequestParamMap);
            if(whereClause != null && !whereClause.equals("")){
                //字符串where不能放在递归函数中处理，不然每递归一次，就会多生成一个where
                whereClause = " where " + whereClause;
            }
            System.out.println(whereClause);
            sqlInfo.setWhereClause(whereClause);
            sqlInfo.setColumnsOfWhereClause(columnsOfWhereClause);
        }
        apiExtensionData.setSqlInfo(sqlInfo);

        apiContext.setApiExtensionData(apiExtensionData);
    }

    private String paramDefaultValueToString(Object object){
        String value = "";
        if (object instanceof Integer || object instanceof Float || object instanceof Double || object instanceof Boolean){
            value = object + "";
        }
        return value;
    }
}
