package com.karst.karstos.crystal2typeb.entities.databese;


/**
 * Created by TianLedao on 2018/4/26.
 */

public class Column {
    public static String DYNAMIC_DEFAULT_VALUE = "dynamic";
    public static String QUERY = "select";
    public static String INSERT = "insert";
    public static String UPDATE = "update";
    public static String CONDITION = "condition";

    private String feature;
    private String belongToTable;
    private String columnName;
    private String columnType;
    private String operator;
    private String columnValue;
    private String alias;
    private String outputFormat;
    private boolean defaultColumn;
    private String defaultValueType;

    public Column(String feature) {
        this.feature = feature;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getBelongToTable() {
        return belongToTable;
    }

    public void setBelongToTable(String belongToTable) {
        this.belongToTable = belongToTable;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getColumnValue() {
        return columnValue;
    }

    public void setColumnValue(String columnValue) {
        this.columnValue = columnValue;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getOutputFormat() {
        return outputFormat;
    }

    public void setOutputFormat(String outputFormat) {
        this.outputFormat = outputFormat;
    }

    public boolean isDefaultColumn() {
        return defaultColumn;
    }

    public void setDefaultColumn(boolean defaultColumn) {
        this.defaultColumn = defaultColumn;
    }

    public String getDefaultValueType() {
        return defaultValueType;
    }

    public void setDefaultValueType(String defaultValueType) {
        this.defaultValueType = defaultValueType;
    }

}
