package com.karst.karstos.crystal2typeb.entities.constants;

public class HttpConstants {
    public static final String API_OPERATION_PARAM_FROM_PATH = "path";
    public static final String API_OPERATION_PARAM_FROM_QUERY = "query";
    public static final String API_OPERATION_PARAM_FROM_HEADER = "header";
    public static final String API_OPERATION_PARAM_FROM_BODY = "body";
    public static final String API_OPERATION_PARAM_FROM_FORMDATA = "formData";
}
