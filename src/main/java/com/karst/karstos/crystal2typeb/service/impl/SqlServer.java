package com.karst.karstos.crystal2typeb.service.impl;

import com.karst.karstos.crystal2typeb.entities.constants.MessageCode;
import com.karst.karstos.crystal2typeb.entities.databese.Column;
import com.karst.karstos.crystal2typeb.entities.databese.DataSource;
import com.karst.karstos.crystal2typeb.entities.response.DbResponseResult;
import com.karst.karstos.crystal2typeb.entities.sql.SqlInfo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by TianLedao on 2018/5/7.
 */
public class SqlServer extends BasicDb {

    @Override
    public DbResponseResult connectDB(String requestPath, DataSource dbConnectInfo) {
        this.jdbcURl = String.format("jdbc:sqlserver://%s:%s;databaseName=%s",
                dbConnectInfo.getHost(), dbConnectInfo.getPort(), dbConnectInfo.getDbName());

        return super.connectDB(requestPath,dbConnectInfo);
    }

    @Override
    public DbResponseResult readRow(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        sqlInfo.setSql("SELECT * FROM (SELECT tableA.*,ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS RN FROM (%s) AS tableA ) AS tableB WHERE RN = 1");
        super.builderSql(sqlInfo);
        dbResponseResult = executeQuery(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        return dbResponseResult;
    }

    @Override
    public DbResponseResult readList(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        if(sqlInfo.isEnablePaging()){
            sqlInfo.setSql("SELECT * FROM (SELECT tableA.*,ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS RN FROM (%s) AS tableA ) AS tableB WHERE RN <= %s AND RN > %s");
        }else{
            sqlInfo.setSql("SELECT * FROM (SELECT tableA.*,ROW_NUMBER() OVER(ORDER BY (SELECT 0)) AS RN FROM (%s) AS tableA ) AS tableB WHERE RN <= 500");
        }
        super.builderSql(sqlInfo);
        dbResponseResult = executeQuery(sqlInfo.getSql());
        dbResponseResult.setCurrentPageNum(sqlInfo.getCurrentPageNum());
        dbResponseResult.setTotalPage(sqlInfo.getTotalPageNum());
        log.info(sqlInfo.getSql());
        if(sqlInfo.getBuildingStstus() != null){
            dbResponseResult.setError(sqlInfo.getBuildingStstus());
            return dbResponseResult;
        }
        return dbResponseResult;
    }

    @Override
    public DbResponseResult insert(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        sqlInfo.setNeedSchema(true);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_CREATE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_CREATE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public DbResponseResult update(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        sqlInfo.setNeedSchema(true);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_UPDATE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_UPDATE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public DbResponseResult delete(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        sqlInfo.setNeedSchema(true);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_DELETE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_DELETE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public <T> T dataTypeConvertsion(ResultSet resultSet, String columnName, String columnTypeName, int dataType) throws SQLException {
        Object result = null;

        switch (dataType){
            //-158是sqlserver中的拓展数据类型，GEOGRAPHY
            //-157是sqlserver中的拓展数据类型，GEOMETRY
            //-155是sqlserver中的拓展数据类型,DATETIMEOFFSET
            //-151是sqlserver中的拓展数据类型，DATETIME
            //-150是sqlserver中的拓展数据类型，SMALLDATETIME
            //-145是sqlserver中的拓展数据类型，GUID
            //999 UNKNOWN类型
            case -158:
            case -157:
            case -155:
            case -151:
            case -150:
            case -145:
            case 999:
                result = resultSet.getString(columnName);
                break;
            //-156是sqlserver中的拓展数据类型，sql_variant,sql_variant 对象可以
            //容纳 SQL Server 所有数据类型（下列类型除外：text、ntext、image、
            // varchar(max)、nvarchar(max)、varbinary(max)、xml、timestamp
            // 和 Microsoft .NET Framework 公共语言运行时 [CLR] 用户定义类型）的数据。
            case -156:
                result = resultSet.getObject(columnName);
                break;
            //-148是sqlserver中的拓展数据类型，MONEY
            //-146是sqlserver中的拓展数据类型，SMALLMONEY
            case -148:
            case -146:
                result = resultSet.getBigDecimal(columnName);
                break;
            //sqlserver中的的blob(2004)字段，这个字段一般存储的是视频、音频等二进制文件
            //microsoft.sql.Types.STRUCTURED(-153),这个主要用于函数和存储过程中，不涉及到查询
            // 都是不打算在水晶中支持的类型
            case -153:
            case 2004:
                result = "不支持的数据类型";
                break;
            default:
                result = super.convertedToJdbcDataType(resultSet,columnName,dataType);
        }

        return (T)result;
    }

    @Override
    public String defaultParameterSetting(Column column) {
        String value = "";
        switch(column.getDefaultValueType()){
            case "timestamp":
                value = "CONVERT(varchar,getdate(),120)";
                break;
            case "current_time":
                value = "CONVERT(varchar,getdate(),108)";
                break;
            case "current_date":
                value = "CONVERT(varchar(10),getdate(),120)";
                break;
            default:
                value = "'" + column.getColumnValue() + "'";
        }
        return value;
    }

    public static String settingColumnName(String columnName,String columnType){
        if (columnType != null && columnType.equals("timestamp")){
            columnName = "convert(datetime," + columnName + ")" + columnName;
        }else{
            columnName = "\"" + columnName + "\"";
        }
        return columnName;
    }

    public static String settingColumnValue(String columnType,String columnValue){
        if(columnType.equalsIgnoreCase("binary")||
                columnType.equalsIgnoreCase("varbinary")||
                columnType.equalsIgnoreCase("varbinary(Max)")){
            columnValue = "CAST('" + columnValue + "' AS VARBINARY)";
        }else if(columnType.equalsIgnoreCase("geography")){
            columnValue = "GEOMETRY::STGeomFromText('" + columnValue + "',0)";
        }else{
            columnValue = "'" + columnValue + "'";
        }
        return columnValue;
    }
}
