package com.karst.karstos.crystal2typeb.entities.response;


import com.karst.karstos.crystal2typeb.entities.error.Error;

import java.util.HashMap;
import java.util.List;

public class DbResponseResult {
    private Integer code;
    private String message;
    private Integer totalPage;
    private Integer currentPageNum;
    private Integer affectedRowNum;
    private Integer count;
    private List<HashMap<String,Object>> resultList;
    private Error error;

    public DbResponseResult() {
    }

    public DbResponseResult(Error error) {
        this.error = error;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getCurrentPageNum() {
        return currentPageNum;
    }

    public void setCurrentPageNum(Integer currentPageNum) {
        this.currentPageNum = currentPageNum;
    }

    public Integer getAffectedRowNum() {
        return affectedRowNum;
    }

    public void setAffectedRowNum(Integer affectedRowNum) {
        this.affectedRowNum = affectedRowNum;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<HashMap<String, Object>> getResultList() {
        return resultList;
    }

    public void setResultList(List<HashMap<String, Object>> resultList) {
        this.resultList = resultList;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
