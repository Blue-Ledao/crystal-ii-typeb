package com.karst.karstos.crystal2typeb.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.karst.karstos.crystal2typeb.entities.SwaggerSpecEntity;

import java.util.concurrent.TimeUnit;

/**
 * Created by TianLedao on 2018/4/25.
 */
public class CrystalCache {

    //swagger配置信息缓存器
    public final static Cache<String,SwaggerSpecEntity> swaggerSpecCache = CacheBuilder.newBuilder()
            //缓存初始大小为10，允许存入10对象
            .initialCapacity(10)
                    //同一时间最多有5个线程往cache执行写操作
            .concurrencyLevel(5)
                    //存活时间为2分钟
            .expireAfterAccess(120, TimeUnit.SECONDS)
            .build();

    //数据库连接字符串缓存器
    public final static Cache<String,String> dbConnectionUrlCache = CacheBuilder.newBuilder()
            //缓存初始大小为10，允许存入10对象
            .initialCapacity(10)
                    //同一时间最多有5个线程往cache执行写操作
            .concurrencyLevel(5)
                    //存活时间为2分钟
            .expireAfterAccess(120, TimeUnit.SECONDS)
            .build();

    //sql语句缓存器
    public final static Cache<String,String> sqlCache = CacheBuilder.newBuilder()
            //缓存初始大小为10，允许存入10对象
            .initialCapacity(10)
                    //同一时间最多有5个线程往cache执行写操作
            .concurrencyLevel(5)
                    //存活时间为2分钟
            .expireAfterAccess(120, TimeUnit.SECONDS)
            .build();

    public static Cache getSwaggerSpecCache(){
        return swaggerSpecCache;
    }

    public static Cache getDbConnectionUrlCache() {
        return dbConnectionUrlCache;
    }

    public static Cache getSqlCache() {
        return sqlCache;
    }
}
