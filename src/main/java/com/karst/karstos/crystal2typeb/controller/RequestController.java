package com.karst.karstos.crystal2typeb.controller;

import com.atlassian.oai.validator.model.SimpleRequest;
import com.google.common.cache.Cache;
import com.karst.karstos.crystal2typeb.configuration.CrystalConfig;
import com.karst.karstos.crystal2typeb.entities.SwaggerSpecEntity;
import com.karst.karstos.crystal2typeb.entities.api.ApiContext;
import com.karst.karstos.crystal2typeb.entities.error.Error;
import com.karst.karstos.crystal2typeb.entities.response.DbResponseResult;
import com.karst.karstos.crystal2typeb.service.ApiParamBinder;
import com.karst.karstos.crystal2typeb.service.ApiValidator;
import com.karst.karstos.crystal2typeb.service.CrystalCache;
import com.karst.karstos.crystal2typeb.service.DatabaseHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by TianLedao on 2018/4/25.
 */
@RestController
public class RequestController {
    @Autowired
    CrystalConfig crystalConfig;

    @Autowired
    ApiValidator apiValidator;

    @Autowired
    ApiParamBinder apiParamBinder;

    @Autowired
    DatabaseHandle databaseHandle;

    @RequestMapping("/**")
    public Object anyRequest(HttpServletRequest request,HttpServletResponse response){
        Map<String,String> queryParameter = new HashMap<>();
        Map<String,String[]> formDataParameter = new HashMap<>(request.getParameterMap());
        separateParameter(request,queryParameter,formDataParameter);

        DbResponseResult dbResponseResult = null;

        //先判断api缓存中是否存在该api对应的swagger信息，
        //这会影响到swagger配置文件是否从远端加载，若缓存中存在，使用缓存中的配置信息，
        // 否则使用从远端加载的配置信息

        /*String requestUri = request.getRequestURI();
        String apiId = requestUri.split("/")[0];*/
        String dbSourceId = request.getHeader("X-DbSourceId");
        String apiId = request.getHeader("X-ApiId");

        SwaggerSpecEntity swaggerSpecEntity = apiValidator.loadSwaggerSpec(apiId, request);

        if (swaggerSpecEntity.getError() != null){
            return swaggerSpecEntity.getError();
        }

        SimpleRequest simpleRequest = apiValidator.buildSimpleRequestModel(request,queryParameter,formDataParameter);

        Error error = apiValidator.validateSwaggerSpec(swaggerSpecEntity, simpleRequest);

        if (null != error){
            response.setStatus(403);
            return error;
        }
        if(!crystalConfig.getEnableSwaggerSpecPath() && apiId != null){
            //验证通过后，判断缓存中是否存在该请求对应的数据库连接信息和sql
            Cache dbConnectionUrlCache = CrystalCache.getDbConnectionUrlCache();
            String jdbcUrl= (String)dbConnectionUrlCache.getIfPresent(dbSourceId);
            Cache sqlCache = CrystalCache.getSqlCache();
            String sql = (String)sqlCache.getIfPresent(apiId);
            //jdbcUrl不为空，则说明这个数据库下的某个api被缓存过
            if(jdbcUrl != null){
                if(sql!=null){
                    //jdbcUrl不为空,sql也不为空，说明这个api可以通过缓存中的数据来执行
                }else{
                    //jdbcUrl不为空，sql为空，说明需要构造一个新的sql
                }
            }
        } else{
            //两种情况下会走这条分支：
            //1.swagger是从本地加载的
            //2.数据库下没有api被缓存过，
            // 这两种情况就会解析swagger数据，构造新的数据库连接器和sql
            ApiContext apiContext = apiValidator.apiHandle(swaggerSpecEntity,simpleRequest);
            apiParamBinder.parseHttpRequestParam(request,apiContext);

            apiParamBinder.parseVendorExtensions(apiContext, request);

            dbResponseResult = databaseHandle.dbHandle(apiContext);

        }

        return dbResponseResult;
    }

    public void separateParameter(HttpServletRequest request,Map<String,String> queryParameter, Map<String, String[]> formDataParameter){
        String queryString = request.getQueryString();
        String name;
        String value;
        if(queryString != null && !queryString.equals("")){
            for(String param:queryString.split("&")){
                String[] paramPairs = param.split("=");
                name=paramPairs[0];
                value = paramPairs[1];
                queryParameter.put(name,value);
            }
        }
        Set<String> querySet = queryParameter.keySet();

        for(String s:querySet){
            if(formDataParameter.containsKey(s)){
                String[] values = formDataParameter.get(s);
                if(values.length>1){
                    for(int i = 0;i<values.length-1;i++){
                        values[i] = values[i+1];
                    }
                    values = Arrays.copyOf(values, values.length - 1);
                    formDataParameter.put(s,values);
                }else{
                    formDataParameter.remove(s);
                }
            }
        }
    }
}
