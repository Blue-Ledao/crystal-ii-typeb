package com.karst.karstos.crystal2typeb.entities.api;


/**
 * Created by TianLedao on 2018/4/26.
 */

public class ApiParameter {
    private String paramName;
    private String paramValue;
    private String paramLocation;

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getParamLocation() {
        return paramLocation;
    }

    public void setParamLocation(String paramLocation) {
        this.paramLocation = paramLocation;
    }
}
