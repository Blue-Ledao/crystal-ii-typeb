package com.karst.karstos.crystal2typeb.service.impl;

import com.karst.karstos.crystal2typeb.entities.constants.MessageCode;
import com.karst.karstos.crystal2typeb.entities.databese.Column;
import com.karst.karstos.crystal2typeb.entities.databese.DataSource;
import com.karst.karstos.crystal2typeb.entities.response.DbResponseResult;
import com.karst.karstos.crystal2typeb.entities.sql.SqlInfo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by TianLedao on 2018/5/7.
 */
public class IBMDB2 extends BasicDb {

    @Override
    public DbResponseResult connectDB(String requestPath, DataSource dbConnectInfo) {
        this.jdbcURl = String.format("jdbc:db2://%s:%s/%s", dbConnectInfo.getHost(), dbConnectInfo.getPort(), dbConnectInfo.getDbName());

        return super.connectDB(requestPath,dbConnectInfo);
    }

    @Override
    public DbResponseResult readRow(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        sqlInfo.setSql("SELECT * FROM (SELECT tableA.*, ROWNUMBER() OVER() AS RN FROM (%s) AS tableA )WHERE RN = 1");
        super.builderSql(sqlInfo);
        dbResponseResult = executeQuery(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        return dbResponseResult;
    }

    @Override
    public DbResponseResult readList(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        if(sqlInfo.isEnablePaging()){
            sqlInfo.setSql("SELECT * FROM (SELECT tableA.*, ROWNUMBER() OVER() AS RN FROM (%s) AS tableA ) WHERE RN <= %s AND RN > %s");
        }else{
            sqlInfo.setSql("SELECT * FROM (SELECT tableA.*, ROWNUMBER() OVER() AS RN FROM (%s) AS tableA ) WHERE RN <= 500");
        }
        super.builderSql(sqlInfo);
        dbResponseResult = executeQuery(sqlInfo.getSql());
        dbResponseResult.setCurrentPageNum(sqlInfo.getCurrentPageNum());
        dbResponseResult.setTotalPage(sqlInfo.getTotalPageNum());
        log.info(sqlInfo.getSql());
        if(sqlInfo.getBuildingStstus() != null){
            dbResponseResult.setError(sqlInfo.getBuildingStstus());
            return dbResponseResult;
        }
        return dbResponseResult;
    }

    @Override
    public DbResponseResult insert(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        sqlInfo.setNeedSchema(true);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_CREATE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_CREATE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public DbResponseResult update(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        sqlInfo.setNeedSchema(true);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_UPDATE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_UPDATE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public DbResponseResult delete(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        sqlInfo.setNeedSchema(true);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_DELETE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_DELETE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public <T> T dataTypeConvertsion(ResultSet resultSet, String columnName, String columnTypeName, int dataType) throws SQLException {
        Object result = null;
        //父类的数据库类型到JDBC数据类型的转换基本覆盖了db2的全部情况
        //SYSPROC.DB2SQLSTATE对应jdbc的DISTINCT，2001，这个类型不打算支持
        switch (dataType){
            default:
                result = super.convertedToJdbcDataType(resultSet,columnName,dataType);
                break;
        }
        return (T)result;
    }

    @Override
    public String defaultParameterSetting(Column column) {
        String value = "";
        switch(column.getDefaultValueType()){
            case "timestamp":
                value = "CURRENT_TIMESTAMP";
                break;
            case "current_time":
                value = "CURRENT_TIME";
                break;
            case "current_date":
                value = "TO_DATE(to_char(sysdate,'yyyy-mm-dd'),'yyyy-mm-dd')";
                break;
            default:
                value = "'" + column.getColumnValue() + "'";
        }
        return value;
    }

    public static String settingColumnName(String columnName){
        return "\"" + columnName + "\"";
    }

    public static String settingColumnValue(String columnType,String columnValue){
        if(columnType.equalsIgnoreCase("BLOB")){
            columnValue = "BLOB('" + columnValue + "')";
        }else{
            columnValue = "'" + columnValue + "'";
        }
        return columnValue;
    }
}
