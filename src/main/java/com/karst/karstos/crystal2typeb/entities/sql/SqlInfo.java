package com.karst.karstos.crystal2typeb.entities.sql;


import com.karst.karstos.crystal2typeb.entities.databese.Column;
import com.karst.karstos.crystal2typeb.entities.error.Error;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TianLedao on 2018/4/26.
 */

public class SqlInfo {
    private String modifyTimestamp;
    private String dbType;
    private String sqltype;
    private boolean needSchema;
    private String schema;
    private String tableType;
    private List<String> tableNames;
    private List<Column> commonColumns = new ArrayList<>();
    private List<Column> columnsOfWhereClause = new ArrayList<>();
    private String whereClause;
    private boolean enablePaging;
    private int endOfQuery;
    private int startOfQuery;
    private int totalPageNum;
    private int currentPageNum;
    private int pageSize;
    private String sql;
    private Error buildingStstus;

    public String getModifyTimestamp() {
        return modifyTimestamp;
    }

    public void setModifyTimestamp(String modifyTimestamp) {
        this.modifyTimestamp = modifyTimestamp;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public String getSqltype() {
        return sqltype;
    }

    public void setSqltype(String sqltype) {
        this.sqltype = sqltype;
    }

    public boolean isNeedSchema() {
        return needSchema;
    }

    public void setNeedSchema(boolean needSchema) {
        this.needSchema = needSchema;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public List<String> getTableNames() {
        return tableNames;
    }

    public void setTableNames(List<String> tableNames) {
        this.tableNames = tableNames;
    }

    public List<Column> getCommonColumns() {
        return commonColumns;
    }

    public void setCommonColumns(List<Column> commonColumns) {
        this.commonColumns = commonColumns;
    }

    public List<Column> getColumnsOfWhereClause() {
        return columnsOfWhereClause;
    }

    public void setColumnsOfWhereClause(List<Column> columnsOfWhereClause) {
        this.columnsOfWhereClause = columnsOfWhereClause;
    }

    public String getWhereClause() {
        return whereClause;
    }

    public void setWhereClause(String whereClause) {
        this.whereClause = whereClause;
    }

    public boolean isEnablePaging() {
        return enablePaging;
    }

    public void setEnablePaging(boolean enablePaging) {
        this.enablePaging = enablePaging;
    }

    public int getEndOfQuery() {
        return endOfQuery;
    }

    public void setEndOfQuery(int endOfQuery) {
        this.endOfQuery = endOfQuery;
    }

    public int getStartOfQuery() {
        return startOfQuery;
    }

    public void setStartOfQuery(int startOfQuery) {
        this.startOfQuery = startOfQuery;
    }

    public int getTotalPageNum() {
        return totalPageNum;
    }

    public void setTotalPageNum(int totalPageNum) {
        this.totalPageNum = totalPageNum;
    }

    public int getCurrentPageNum() {
        return currentPageNum;
    }

    public void setCurrentPageNum(int currentPageNum) {
        this.currentPageNum = currentPageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Error getBuildingStstus() {
        return buildingStstus;
    }

    public void setBuildingStstus(Error buildingStstus) {
        this.buildingStstus = buildingStstus;
    }
}
