package com.karst.karstos.crystal2typeb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrystalⅡTypeBApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrystalⅡTypeBApplication.class, args);
	}
}
