package com.karst.karstos.crystal2typeb.service.impl;

import com.karst.karstos.crystal2typeb.entities.constants.MessageCode;
import com.karst.karstos.crystal2typeb.entities.databese.Column;
import com.karst.karstos.crystal2typeb.entities.databese.DataSource;
import com.karst.karstos.crystal2typeb.entities.response.DbResponseResult;
import com.karst.karstos.crystal2typeb.entities.sql.SqlInfo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by TianLedao on 2018/4/28.
 */
public class Mysql extends BasicDb{

    @Override
    public DbResponseResult connectDB(String requestPath, DataSource dbConnectInfo) {
        this.jdbcURl = String.format("jdbc:mysql://%s:%s/%s?useUnicode=true&characterEncoding=utf8", dbConnectInfo.getHost(), dbConnectInfo.getPort(), dbConnectInfo.getDbName());

        return super.connectDB(requestPath,dbConnectInfo);
    }

    @Override
    public DbResponseResult readRow(SqlInfo sqlInfo) {
        /*DbResponseResult dbResponseResult = null;
        List<Column> columnList = sqlInfo.getCommonColumns();
        String querySql = "select ";
        for(int i = 0;i < columnList.size();i++){
            Column column = columnList.get(i);
            if(i > 0){
                querySql += ",";
            }
            querySql +=  column.getColumnName() + " as " + column.getAlias();
        }
        querySql += " from " + sqlInfo.getTableNames().get(0) + sqlInfo.getWhereClause();
        System.out.println(querySql);
        return dbResponseResult;*/

        DbResponseResult dbResponseResult = null;
        sqlInfo.setSql("%s limit 1");
        super.builderSql(sqlInfo);
        dbResponseResult = executeQuery(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        return dbResponseResult;
    }

    @Override
    public DbResponseResult readList(SqlInfo sqlInfo) {
        /*DbResponseResult dbResponseResult = null;
        List<Column> columnList = sqlInfo.getCommonColumns();
        String querySql = "select ";
        String tableName = "";
        if (sqlInfo.isNeedSchema()){
            for(int i = 0; i<sqlInfo.getTableNames().size();i++){
                if(i>1){
                    tableName += ",";
                }
                tableName += sqlInfo.getSchema() + "." + sqlInfo.getTableNames().get(i);
            }
        }else {
            for(int i = 0; i<sqlInfo.getTableNames().size();i++){
                if(i>1){
                    tableName += ",";
                }
                tableName += sqlInfo.getTableNames().get(i);
            }
        }
        if (sqlInfo.isEnablePaging()){
            countTotalPageNum(sqlInfo,tableName);
        }
        for(int i = 0;i < columnList.size();i++){
            Column column = columnList.get(i);
            if(i > 0){
                querySql += ",";
            }
            querySql +=  column.getColumnName() + " as " + column.getAlias();
        }
        if(sqlInfo.isEnablePaging()){
            querySql += " from " + sqlInfo.getTableNames().get(0) + sqlInfo.getWhereClause() + "limit " + sqlInfo.getStartOfQuery() + "," + sqlInfo.getEndOfQuery() ;
        }else{
            querySql += " from " + sqlInfo.getTableNames().get(0) + sqlInfo.getWhereClause();
        }
        System.out.println(querySql);
        return dbResponseResult;*/
        DbResponseResult dbResponseResult = null;
        if(sqlInfo.isEnablePaging()){
            sqlInfo.setSql("%s limit %s,%s");
        }else{
            sqlInfo.setSql("%s limit 500");
        }
        super.builderSql(sqlInfo);
        dbResponseResult = executeQuery(sqlInfo.getSql());
        dbResponseResult.setCurrentPageNum(sqlInfo.getCurrentPageNum());
        dbResponseResult.setTotalPage(sqlInfo.getTotalPageNum());
        log.info(sqlInfo.getSql());
        if(sqlInfo.getBuildingStstus() != null){
            dbResponseResult.setError(sqlInfo.getBuildingStstus());
            return dbResponseResult;
        }
        return dbResponseResult;
    }

    @Override
    public DbResponseResult insert(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult;
        sqlInfo.setNeedSchema(false);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_CREATE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_CREATE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public DbResponseResult update(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult;
        sqlInfo.setNeedSchema(false);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_UPDATE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_UPDATE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public DbResponseResult delete(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult;
        sqlInfo.setNeedSchema(false);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_DELETE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_DELETE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public <T> T dataTypeConvertsion(ResultSet resultSet, String columnName, String columnTypeName, int dataType) throws SQLException {
        Object result = null;

        //switch默认走父类的数据转换方法，虽然父类包含了了绝大多数JDBC到java数据类型的转换，
        //但是仍然有一些数据库字段需要单独处理，因此在子类和父类的switch语句中看到了相同的case
        //不要认为这被重复定义了，而且子类中的case通常不会break;只有case里的if语句或者case只
        // 存在一种情况时里面才会break；目的是当这些字段不需要单独处理时能够进入default语句，
        //走父类定义的转换逻辑，只有特殊的字段被处理完了才会break;
        switch (dataType){
            case -7:
                result = resultSet.getString(columnName);
                break;
            default:
                result = super.convertedToJdbcDataType(resultSet,columnName,dataType);
                break;
        }
        return (T)result;
    }

    @Override
    public String defaultParameterSetting(Column column) {
        String value = "";
        switch(column.getDefaultValueType()){
            case "timestamp":
                value = "NOW()";
                break;
            case "current_date":
                value = "CURDATE()";
                break;
            case "current_time":
                value = "CURTIME()";
                break;
            default:
                value = "'" + column.getColumnValue() + "'";
        }
        return value;
    }

    public static String settingColumnName(String columnName){
        return "`" + columnName + "`";
    }

    public static String settingColumnValue(String columnType,String columnValue){
        if (columnType.equals("bit")){
            columnValue = "b'" + columnValue + "'";
        }else{
            columnValue = "'" + columnValue + "'";
        }
        return columnValue;
    }
}
