package com.karst.karstos.crystal2typeb.service.impl;

import com.karst.karstos.crystal2typeb.entities.constants.MessageCode;
import com.karst.karstos.crystal2typeb.entities.databese.Column;
import com.karst.karstos.crystal2typeb.entities.databese.DataSource;
import com.karst.karstos.crystal2typeb.entities.response.DbResponseResult;
import com.karst.karstos.crystal2typeb.entities.sql.SqlInfo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TianLedao on 2018/5/7.
 */
public class Oracle extends BasicDb {

    @Override
    public DbResponseResult connectDB(String requestPath, DataSource dbConnectInfo) {
        this.jdbcURl = String.format("jdbc:oracle:thin:@//%s:%s/%s", dbConnectInfo.getHost(), dbConnectInfo.getPort(), dbConnectInfo.getDbName());

        return super.connectDB(requestPath,dbConnectInfo);
    }

    @Override
    public DbResponseResult readRow(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        sqlInfo.setSql("SELECT * FROM (%s) WHERE ROWNUM = 1");
        super.builderSql(sqlInfo);
        dbResponseResult = executeQuery(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        return dbResponseResult;
    }

    @Override
    public DbResponseResult readList(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        if(sqlInfo.isEnablePaging()){
            sqlInfo.setSql("SELECT * FROM (SELECT A.*, ROWNUM RN FROM (%s) A WHERE ROWNUM <= %s) WHERE RN > %s");
        }else{
            sqlInfo.setSql("SELECT * FROM (%s) WHERE ROWNUM <= 500");
        }
        super.builderSql(sqlInfo);
        dbResponseResult = executeQuery(sqlInfo.getSql());
        dbResponseResult.setCurrentPageNum(sqlInfo.getCurrentPageNum());
        dbResponseResult.setTotalPage(sqlInfo.getTotalPageNum());
        log.info(sqlInfo.getSql());
        if(sqlInfo.getBuildingStstus() != null){
            dbResponseResult.setError(sqlInfo.getBuildingStstus());
            return dbResponseResult;
        }
        return dbResponseResult;
    }

    @Override
    public DbResponseResult insert(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult;
        sqlInfo.setNeedSchema(true);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_CREATE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_CREATE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public DbResponseResult update(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        sqlInfo.setNeedSchema(true);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_UPDATE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_UPDATE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public DbResponseResult delete(SqlInfo sqlInfo) {
        DbResponseResult dbResponseResult = null;
        sqlInfo.setNeedSchema(true);
        super.builderSql(sqlInfo);
        dbResponseResult = executeWriter(sqlInfo.getSql());
        log.info(sqlInfo.getSql());
        if (dbResponseResult.getError()!=null){
            return dbResponseResult;
        }
        dbResponseResult.setMessage(MessageCode.SUCCESSFULLY_DELETE_DATA);
        dbResponseResult.setCode(MessageCode.SUCCESSFULLY_DELETE_DATA_CODE);
        return dbResponseResult;
    }

    @Override
    public <T> T dataTypeConvertsion(ResultSet resultSet, String columnName, String columnTypeName, int dataType) throws SQLException {
        Object result = null;

        switch (dataType){
            //-104和-103都不是标准jdbc里面定义的，而是oracle的拓展数据类型，
            //INTERVAL DAY TO SECOND 和 INTERVALYEAR TO MONTH
            //oracle在实现jdbc的时候给拓展类型设置了编号。
            case -104:
            case -103:
                //-101和-101都不是标准jdbc里面定义的，而是oracle的拓展数据类型，
                //TIMESTAMP WITH TIME 和 ZONE TIMESTAMP WITH LOCAL TIME ZONE
                //oracle在实现jdbc的时候给拓展类型设置了编号。
            case -102:
            case -101:
                result = resultSet.getString(columnName);
                break;
            //oracle拓展的数据库类型binary_float
            case 100:
                result = resultSet.getFloat(columnName);
                break;
            //oracle拓展的数据库类型binary_double
            case 101:
                result = resultSet.getDouble(columnName);
                break;
            //oracle的blob字段，这个字段一般存储的是视频、音频等二进制文件
            case 2004:
                result = "不支持的数据类型";
                break;
            default:
                result = super.convertedToJdbcDataType(resultSet,columnName,dataType);
        }

        return (T)result;
    }

    @Override
    public String defaultParameterSetting(Column column) {
        String value = "";
        switch(column.getDefaultValueType()){
            case "current_time":
                value = "SYSDATE";
                break;
            case "timestamp":
                value = "SYSTIMESTAMP";
                break;
            case "current_date":
                value = "TO_DATE(to_char(sysdate,'yyyy-mm-dd'),'yyyy-mm-dd')";
                break;
            default:
                value = "'" + column.getColumnValue() + "'";
        }
        return value;
    }

    public static String settingColumnName(String columnName){
        return "\"" + columnName + "\"";
    };

    public static String settingColumnValue(String columnType,String columnValue){
        Pattern pattern;
        Matcher matcher;
        /*String[] regExs = {
                "TIMESTAMP(\\d+)||TIMESTAMP(\\d+) WITH TIME ZONE||TIMESTAMP(\\d+) WITH LOCAL TIME ZONE",
                "INTERVAL YEAR(\\d+) TO MONTH",
                "INTERVAL DAY(\\d+) TO SECOND(\\d+)"
        };*/

        if (columnType.contains("TIMESTAMP")){
            columnType = "TIMESTAMP";
        }
        if (columnType.contains("INTERVAL YEAR")){
            columnType = "TIMESTAMP YEAR TO MONTH";
        }
        if (columnType.contains("INTERVAL DAY")){
            columnType = "TIMESTAMP DAY TO SECOND";
        }
        switch(columnType){
            case "DATE":
            case "TIMESTAMP":
                pattern = Pattern.compile("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}|\\d{4}/\\d{1,2}/\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}");
                matcher = pattern.matcher(columnValue);
                if(matcher.matches()){
                    columnValue = "to_date('" + columnValue + "','YYYY-MM-DD hh24:mi:ss')";
                }else{
                    columnValue = "to_date('" + columnValue + "','YYYY-MM-DD')";
                }
                break;
            case "RAW":
                columnValue = "hextoraw('" + columnValue +"')";
                break;
            case "TIMESTAMP YEAR TO MONTH":
            case "TIMESTAMP DAY TO SECOND":
                break;
            default:
                columnValue = "'" + columnValue + "'";
        }
        return columnValue;
    }
}
