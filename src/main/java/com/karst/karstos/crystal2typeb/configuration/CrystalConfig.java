package com.karst.karstos.crystal2typeb.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
/**
 * Created by TianLedao on 2018/4/25.
 */
@Component
@ConfigurationProperties
@PropertySource("classpath:application.properties")
public class CrystalConfig {
    private String ApigatewayExtensionsName;
    private Boolean enableSwaggerSpecPath;
    private String swaggerSpecIdHeader;
    private String swaggerSpecVersionHeader;
    private String swaggerSpecEndpoint;
    private String swaggerSpecPath;

    public String getApigatewayExtensionsName() {
        return ApigatewayExtensionsName;
    }

    public void setApigatewayExtensionsName(String apigatewayExtensionsName) {
        ApigatewayExtensionsName = apigatewayExtensionsName;
    }

    public Boolean getEnableSwaggerSpecPath() {
        return enableSwaggerSpecPath;
    }

    public void setEnableSwaggerSpecPath(Boolean enableSwaggerSpecPath) {
        this.enableSwaggerSpecPath = enableSwaggerSpecPath;
    }

    public String getSwaggerSpecIdHeader() {
        return swaggerSpecIdHeader;
    }

    public void setSwaggerSpecIdHeader(String swaggerSpecIdHeader) {
        this.swaggerSpecIdHeader = swaggerSpecIdHeader;
    }

    public String getSwaggerSpecVersionHeader() {
        return swaggerSpecVersionHeader;
    }

    public void setSwaggerSpecVersionHeader(String swaggerSpecVersionHeader) {
        this.swaggerSpecVersionHeader = swaggerSpecVersionHeader;
    }

    public String getSwaggerSpecEndpoint() {
        return swaggerSpecEndpoint;
    }

    public void setSwaggerSpecEndpoint(String swaggerSpecEndpoint) {
        this.swaggerSpecEndpoint = swaggerSpecEndpoint;
    }

    public String getSwaggerSpecPath() {
        return swaggerSpecPath;
    }

    public void setSwaggerSpecPath(String swaggerSpecPath) {
        this.swaggerSpecPath = swaggerSpecPath;
    }
}
