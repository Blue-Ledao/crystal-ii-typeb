package com.karst.karstos.crystal2typeb.service;

import com.karst.karstos.crystal2typeb.entities.api.ApiExtensionData;
import com.karst.karstos.crystal2typeb.entities.databese.DataSource;
import com.karst.karstos.crystal2typeb.entities.response.DbResponseResult;
import com.karst.karstos.crystal2typeb.entities.sql.SqlInfo;

import java.sql.Connection;

/**
 * Created by TianLedao on 2018/4/28.
 */
public interface DataBase {

    public Connection connection = null;

    public DbResponseResult connectDB(String requestPath,DataSource dbConnectInfo);
    public DbResponseResult readRow(SqlInfo sqlInfo);
    public DbResponseResult readList(SqlInfo sqlInfo);
    public DbResponseResult insert(SqlInfo sqlInfo);
    public DbResponseResult update(SqlInfo sqlInfo);
    public DbResponseResult delete(SqlInfo sqlInfo);
}
