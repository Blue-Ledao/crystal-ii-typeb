package com.karst.karstos.crystal2typeb.service.columns;

import com.karst.karstos.crystal2typeb.entities.api.ApiContext;
import com.karst.karstos.crystal2typeb.entities.databese.Column;
import com.karst.karstos.crystal2typeb.service.impl.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by TianLedao on 2018/4/27.
 */
@Service
public class BindColumnsData {

    public List<Column> buildColumnsTypeOfQuery(String dbType,List<Map<String,Object>> columns){
        List<Column> columnList = new ArrayList<>();
        for(Map<String,Object> columnMap:columns){
            Column column = new Column(Column.QUERY);
            column.setBelongToTable(columnMap.get("belongToTable") != null ? (String) columnMap.get("belongToTable") : null);
            String columnName = (String)columnMap.get("columnName");
            String columnType = (String)columnMap.get("columnType");
            columnName = meteDataPreprocess(dbType,columnName,columnType,null);
            column.setColumnName(columnName);
            column.setColumnType(columnType);
            column.setAlias((String)columnMap.get("alias"));
            column.setOutputFormat((String)columnMap.get("outputFormat"));
            columnList.add(column);
        }
        return columnList;
    }

    public List<Column> buildColumnsTypeOfCreate(String dbType,List<Map<String,Object>> columns,ApiContext apiContext){
        List<Column> columnList = new ArrayList<>();
        for(Map<String,Object> columnMap:columns){
            Column column = new Column(Column.INSERT);
            String columnName = (String)columnMap.get("columnName");
            String columnType = (String)columnMap.get("columnType");
            columnName = meteDataPreprocess(dbType,columnName,columnType,null);
            column.setColumnName(columnName);
            column.setColumnType(columnType);
            //判断该Column是否被设置成了默认Column,
            // 默认column的value和非默认column的处理方式不同
            if(columnMap.containsKey("defaultColumn")){
                column.setDefaultColumn((Boolean) columnMap.get("defaultColumn"));
                String value = meteDataPreprocess(dbType,null,columnType,(String)columnMap.get("value"));
                column.setColumnValue(value);
                if(columnMap.get("defaultValueType").equals(Column.DYNAMIC_DEFAULT_VALUE)){
                    column.setColumnValue((String)columnMap.get("value"));
                }
                continue;
            }
            String paramName = (String)columnMap.get("valueFromParam");
            String paramLocation = (String)columnMap.get("paramLocation");
            String value = apiContext.getApiRequestParam(paramName,paramLocation);
            value = meteDataPreprocess(dbType,null,columnType,value);
            column.setColumnValue(value);
            if(value != null && !value.equals("")){
                columnList.add(column);
            }
        }
        return columnList;
    }

    public List<Column> buildColumnsTypeOfUpdate(String dbType,List<Map<String,Object>> columns,ApiContext apiContext){
        List<Column> columnList = new ArrayList<>();
        for(Map<String,Object> columnMap:columns){
            Column column = new Column(Column.UPDATE);
            String columnName = (String)columnMap.get("columnName");
            String columnType = (String)columnMap.get("columnType");
            columnName = meteDataPreprocess(dbType,columnName,columnType,null);
            column.setColumnName(columnName);
            column.setColumnType(columnType);
            String paramName = (String)columnMap.get("valueFromParam");
            String paramLocation = (String)columnMap.get("paramLocation");
            String value = apiContext.getApiRequestParam(paramName,paramLocation);
            value = meteDataPreprocess(dbType,null,columnType,value);
            column.setColumnValue(value);
            if(value != null && !value.equals("")){
                columnList.add(column);
            }
        }
        return columnList;
    }

    public String bindParamForWhereClause(String dbType,List<Map<String,?>> conditions,List<Column> columns,Map<String, String> apiRequestParamMap){
        String where = "";
        String logic = "";
        for(int i=0;i<conditions.size();i++){
            String basicCondition = "";
            Map<String, ?> m = conditions.get(i);
            if(m.containsKey("group")){
                basicCondition = bindParamForWhereClause(dbType,(List<Map<String, ?>>) m.get("group"), columns, apiRequestParamMap);
            }
            else{
                if(m.containsKey("logicOperator")){
                    logic = " " +  m.get("logicOperator") + " ";
                    where += logic;
                    continue;
                }
                Map<String,?> leftExpressMap =  (Map<String,?>)m.get("leftExpression");
                Map<String,?> rightExpressMap =  (Map<String,?>)m.get("rightExpression");
                String leftExpress,rightExpress=null;
                Column column = new Column(Column.CONDITION);

                String belongToTable = (String) leftExpressMap.get("belongToTable");
                column.setBelongToTable(meteDataPreprocess(dbType, belongToTable, null, null));

                String columnName = (String) leftExpressMap.get("columnName");
                column.setColumnName(meteDataPreprocess(dbType, columnName, null, null));

                String columnType = (String) leftExpressMap.get("columnType");
                column.setColumnType(meteDataPreprocess(dbType, columnType, null,null));

                column.setOperator(BasicDb.getOperator((String) m.get("operator")));



                leftExpress = column.getBelongToTable() +"." + column.getColumnName();

                //表达式右边是列名的情况，即：tableName.columnA = tableName.columnB
                if(rightExpressMap.containsKey("columnName")&&rightExpressMap.containsKey("belongToTable")){
                    rightExpress = meteDataPreprocess(dbType, (String) rightExpressMap.get("belongToTable"), null, null) +"." +
                            meteDataPreprocess(dbType, (String) rightExpressMap.get("columnName"), null, null);
                    //column.setColumnValue(rightExpress);
                }else{
                    String paramName = (String) rightExpressMap.get("valueFromParam");
                    String paramLocation = (String) rightExpressMap.get("paramLocation");
                    String paramKey = paramName + "&" + paramLocation;

                    if(apiRequestParamMap.containsKey(paramKey)){
                        rightExpress = apiRequestParamMap.get(paramKey);
                        column.setColumnValue(rightExpress);
                    }
                }
                columns.add(column);
                if(column.getColumnValue()!=null &&!column.getColumnValue().equals("")){
                    operatorAndColumnValuePreprocess(dbType, column);
                    basicCondition = leftExpress + " " + column.getOperator() + " " + column.getColumnValue() ;
                }else{
                    basicCondition = leftExpress + " " + column.getOperator() + " " + rightExpress;
                }
            }
            where += "(" + basicCondition + ")";
        }
        return where;
    }

    public String meteDataPreprocess(String databaseType,String columnNameOrTableName,String columnType,String columnValue){
        String result = "";
        switch(databaseType){
            case "mysql":
                if(columnNameOrTableName != null && !columnNameOrTableName.equals("")){
                    result = Mysql.settingColumnName(columnNameOrTableName);
                }
                if(columnValue != null){
                    result = Mysql.settingColumnValue(columnType,columnValue);
                }
                break;
            case "oracle":
                if(columnNameOrTableName != null && !columnNameOrTableName.equals("")){
                    result = Oracle.settingColumnName(columnNameOrTableName);
                }
                if(columnValue != null){
                    result = Oracle.settingColumnValue(columnType,columnValue);
                }
                break;
            case "mssql":
                if(columnNameOrTableName != null && !columnNameOrTableName.equals("")){
                    result = SqlServer.settingColumnName(columnNameOrTableName, columnType);
                }
                if(columnValue != null){
                    result = SqlServer.settingColumnValue(columnType,columnValue);
                }
                break;
            case "db2":
                if(columnNameOrTableName != null && !columnNameOrTableName.equals("")){
                    result = IBMDB2.settingColumnName(columnNameOrTableName);
                }
                if(columnValue != null){
                    result = IBMDB2.settingColumnValue(columnType,columnValue);
                }
                break;
            case "pgsql":
                if(columnNameOrTableName != null && !columnNameOrTableName.equals("")){
                    result = PostgreSQL.settingColumnName(columnNameOrTableName);
                }
                if(columnValue != null){
                    result = PostgreSQL.settingColumnValue(columnType,columnValue);
                }
                break;
        }
        return result;
    }

    public void operatorAndColumnValuePreprocess(String dbType,Column column){
        String columnValue = column.getColumnValue();
        String operator = column.getOperator();
        String columnType = column.getColumnType();
        if(columnValue.equalsIgnoreCase("null")){
            columnValue = "is null";
        }else{
            switch(operator){
                case "contain":
                    operator = "";
                    columnValue = " LIKE '%" + columnValue + "%'";
                    break;
                case "left_contain":
                    operator = "";
                    columnValue = " LIKE '" + columnValue + "%'";
                    break;
                case "right_contain":
                    operator = "";
                    columnValue = " LIKE '%" + columnValue + "'";
                    break;
                default:
                    meteDataPreprocess(dbType,null,columnType,columnValue);
                    columnValue = "'" + columnValue + "'";
            }
        }
        column.setOperator(operator);
        column.setColumnValue(columnValue);
    }

}
