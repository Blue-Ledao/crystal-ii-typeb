package com.karst.karstos.crystal2typeb.entities.error;

import java.util.Date;
/**
 * Created by TianLedao on 2018/4/25.
 */
public class Error {
    private String message;
    private Integer errorCode;
    private String errorType;
    private String requestPath;
    private Date timestamp;

    public Error() {
    }

    public Error(String message, int errorCode, String errorType,
                 String requestPath, Date timestamp) {
        this.message = message;
        this.errorCode = errorCode;
        this.errorType = errorType;
        this.requestPath = requestPath;
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
