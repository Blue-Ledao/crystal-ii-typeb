package com.karst.karstos.crystal2typeb.service;

import com.atlassian.oai.validator.SwaggerRequestResponseValidator;
import com.atlassian.oai.validator.interaction.ApiOperationResolver;
import com.atlassian.oai.validator.model.ApiOperationMatch;
import com.atlassian.oai.validator.model.Request;
import com.atlassian.oai.validator.model.SimpleRequest;
import com.atlassian.oai.validator.report.ValidationReport;
import com.google.common.cache.Cache;
import com.karst.karstos.crystal2typeb.configuration.CrystalConfig;
import com.karst.karstos.crystal2typeb.entities.SwaggerSpecEntity;
import com.karst.karstos.crystal2typeb.entities.api.ApiContext;
import com.karst.karstos.crystal2typeb.entities.error.Error;
import com.karst.karstos.crystal2typeb.entities.constants.MessageCode;
import com.karst.karstos.crystal2typeb.tools.Utils;
import io.swagger.models.Swagger;
import io.swagger.parser.SwaggerParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;

/**
 * Created by TianLedao on 2018/4/25.
 */
@Service
public class ApiValidator {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CrystalConfig crystalConfig;

    //用于发送http请求的对象
    private RestTemplate restTemplate = new RestTemplate();

    public SwaggerSpecEntity loadSwaggerSpec(String apiId,HttpServletRequest request){

        boolean enableSwaggerSpecPath = crystalConfig.getEnableSwaggerSpecPath();
        String swaggerContext = "";
        Cache swaggerSpecCache = CrystalCache.getSwaggerSpecCache();
        if(enableSwaggerSpecPath){
            //从本地配置中加载swagger
            String path = crystalConfig.getSwaggerSpecPath();
            try{
                swaggerContext = Utils.readFile(path);
            }catch (Exception e) {
                e.printStackTrace();
                logger.error("错误："+e.toString());
            }
        }else{
            //从缓存中加载swagger

            SwaggerSpecEntity swaggerSpecEntity = (SwaggerSpecEntity) swaggerSpecCache.getIfPresent(apiId);
            if(swaggerSpecEntity != null){
                return swaggerSpecEntity;
            }
            try{
                String swaggerSpecEndpoint = crystalConfig.getSwaggerSpecEndpoint();
                if(!swaggerSpecEndpoint.endsWith("/")){
                    swaggerSpecEndpoint += "/";
                }
                swaggerContext = restTemplate.getForObject(swaggerSpecEndpoint + apiId, String.class);
            }catch (Exception e){
                logger.debug("错误：" + e.toString());
            }
        }
        SwaggerSpecEntity swaggerSpecEntity = new SwaggerSpecEntity(swaggerContext);
        //本地加载的swagger配置文件，没有apiId，不用进行缓存
        if(!crystalConfig.getEnableSwaggerSpecPath() && apiId != null){
            swaggerSpecCache.put(apiId, swaggerSpecEntity);
        }
        return swaggerSpecEntity;
    }

    public SimpleRequest buildSimpleRequestModel(HttpServletRequest request,Map<String,String> queryParameter,Map<String,String[]> formDataParameter){
        SimpleRequest.Builder requestBuilder = null;
        String paramName = null;
        String paramValue = null;
        switch(request.getMethod()){
            case "GET":
                requestBuilder = SimpleRequest.Builder.get(request.getRequestURI());
                for(Enumeration<String> e = request.getParameterNames();e.hasMoreElements();){
                    paramName =e.nextElement();
                    paramValue = queryParameter.get(paramName);
                    requestBuilder.withQueryParam(paramName, paramValue);
                }
                break;
            case "POST":
                String formData = "";
                requestBuilder = SimpleRequest.Builder.post(request.getRequestURI());
                for(Map.Entry<String, String> entry : queryParameter.entrySet()){
                    requestBuilder.withQueryParam(entry.getKey(),entry.getValue());
                }
                for(Map.Entry<String, String[]> entry : formDataParameter.entrySet()){
                    String[] values = entry.getValue();
                    paramName = entry.getKey();
                    if(values.length > 1){
                        for(String value : values){
                            formData += paramName + "=" + value + "&";
                        }
                    }else{
                        paramValue = values[0];
                        formData += paramName + "=" + paramValue + "&";
                    }
                }
                if(!formData.equals("")){
                    int index = formData.lastIndexOf("&");
                    formData = formData.substring(0,index);
                    requestBuilder.withBody(formData.substring(0,index));
                }
                break;
            case "DELETE":
                requestBuilder = SimpleRequest.Builder.delete(request.getRequestURI());
                for(Enumeration<String> e = request.getParameterNames();e.hasMoreElements();){
                    paramName =e.nextElement();
                    paramValue = request.getParameter(paramName);
                    requestBuilder.withQueryParam(paramName, paramValue);
                }
                break;
        }
        //构造header参数（水晶里面不会出现body参数，因此不用构造body参数）
        for(Enumeration<String> e = request.getHeaderNames();e.hasMoreElements();){
            paramName =e.nextElement();
            paramValue = request.getHeader(paramName);
            requestBuilder.withHeader(paramName, paramValue);
        }
        return requestBuilder.build();
    }

    public Error validateSwaggerSpec(SwaggerSpecEntity swaggerSpecEntity, Request request){
        SwaggerRequestResponseValidator validator = swaggerSpecEntity.getValidator();
        ValidationReport report = validator.validateRequest(request);
        if (report.hasErrors()) {
            String message = "";
            for (ValidationReport.Message m : report.getMessages()) {
                message += m.getMessage() + " ";
            }
            Error error = new Error();
            error.setTimestamp(new Date());
            error.setMessage(message);
            error.setRequestPath(request.getPath());
            error.setErrorCode(MessageCode.SWAGGER_VALIDATION_FAILURE_CODE);
            error.setErrorType(MessageCode.SWAGGER_VALIDATION_FAILURE);
            return error;
        }
        return null;
    }

    public ApiContext apiHandle(SwaggerSpecEntity swaggerSpecEntity, Request request){
        Swagger swagger = new SwaggerParser().parse(swaggerSpecEntity.getSwaggerContext());
        ApiOperationResolver apiOperationResolver = new ApiOperationResolver(swagger, null);
        ApiOperationMatch apiOpMatch = apiOperationResolver.findApiOperation(request.getPath(), request.getMethod());
        if (!apiOpMatch.isOperationAllowed()) {
            logger.error("can not find op url:" + request.getPath() + " method:" + request.getMethod());
        }
        ApiContext apiContext = new ApiContext();
        apiContext.setSwaggerVendorExtensions(swagger.getVendorExtensions());
        apiContext.setApiOperation(apiOpMatch.getApiOperation());
        apiContext.setRequestPath(request.getPath());
        return apiContext;
    }
}
